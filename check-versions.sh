# script to check if the versions of images that are
# changed and not staged exist in the registries
# optionally use ./check-versions.sh --cached

# shellcheck disable=SC2086
images=$(git diff $1 roles/librecoop.deploy/defaults/main.yml \
| grep -E '^\+.+image_version' \
| sed -r 's/\+(.+)_image_version: "(.+)"/\1:\2/' \
| sed 's|traefik:|traefik:v|' \
| sed 's|backupbot:|registry.gitlab.com/librecoop/backup-bot-two:v|' \
| sed -r 's|(nextcloud.+)|\1-apache|' \
| sed 's|onlyoffice|onlyoffice/documentserver|' \
| sed 's|mailu|ghcr.io/mailu/admin|' \
| sed -r 's|(tika.+)|apache/\1-full|' \
| sed 's|clamav|clamav/clamav-debian|' \
| sed 's|vaultwarden|vaultwarden/server|' \
| sed 's|seafile:|seafileltd/seafile-mc:|' \
| sed 's|seafile_pro|docker.seadrive.org/seafileltd/seafile-pro-mc|' \
| sed 's|elasticsearch|docker.elastic.co/elasticsearch/elasticsearch|' \
| sed 's|grafana|grafana/grafana|' \
| sed 's|loki|grafana/loki|' \
| sed 's|prometheus:|prom/prometheus:v|' \
| sed 's|node_exporter:|prom/node-exporter:v|' \
| sed 's|cadvisor:|gcr.io/cadvisor/cadvisor:v|' \
| sed -r 's|(redis.+)|\1-alpine|' \
| sed 's|ql_13||;s|ql_15||' | sed -r 's|(postgres.+)|\1-alpine|' \
| sed -r 's|blackbox:|quay.io/prometheus/blackbox-exporter:v|')

# wordpress, mariadb and memcached don't need to be
# replaced because the images are called the same

for image in $images; do
  echo -n "$image "
  docker manifest inspect "$image" >/dev/null && echo Ok!
done

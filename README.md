# librecloud-ansible

Librecoop's ansible roles for docker-compose-based deployment

TBD

## Unlock vaults

requires [rbw](https://github.com/doy/rbw)

`ansible-vault view ./inventory/host_vars/dev.librecoop.es/vault --vault-id=dev@vault-client.sh`

`ansible-vault view ./inventory/host_vars/proood.librecoop.es/vault --vault-id=proood@vault-client.sh`

## View backups

```bash
export $(ansible-vault view ./inventory/host_vars/dev.librecoop.es/vault --vault-id='Ansible vault dev@vault-client.sh' | awk -v single_quote="'" '/restic_password/{gsub(single_quote, "", $2); print "RESTIC_PASSWORD="$2} /b2_account_id/{gsub(single_quote, "", $2); print "AWS_ACCESS_KEY_ID="$2} /b2_account_key/{gsub(single_quote, "", $2); print "AWS_SECRET_ACCESS_KEY="$2}' | xargs )
restic -r s3:s3.us-east-005.backblazeb2.com/backup-librecloud-dev/dev-$USER snapshots
```

```bash
export $(ansible-vault view ./inventory/host_vars/proood.librecoop.es/vault --vault-id='Ansible vault proood@vault-client.sh' | awk -v single_quote="'" '/restic_password/{gsub(single_quote, "", $2); print "RESTIC_PASSWORD="$2} /b2_account_id/{gsub(single_quote, "", $2); print "AWS_ACCESS_KEY_ID="$2} /b2_account_key/{gsub(single_quote, "", $2); print "AWS_SECRET_ACCESS_KEY="$2}' | xargs )
restic -r s3:s3.us-east-005.backblazeb2.com/backup-librecloud-prod/proood.librecoop.es snapshots
```

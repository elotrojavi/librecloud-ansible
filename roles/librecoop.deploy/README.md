Deploy Role
===================

This role deploys a Librecoops cloud that consists of:

- traefik
- backup-bot-two
- odoo
- nextcloud
- onlyoffice
- mailu
- wordpress
- vaultwarden
- seafile
- grafana

After running
-------------

For onlyoffice in nextcloud, after deploying:
- install the ONLYOFFICE app
- in the onlyoffice settings:
  - set `ONLYOFFICE Docs address` to `https://{{ onlyoffice_host }}/`
  - set `Secret key` to `{{ onlyoffice_secret_key }}`

After deploying seafile you might want to change a few options:

- in `seafile.conf`
```
[fileserver]
max_sync_file_count = -1
fs_id_list_request_timeout = -1
```
```
[notification]
enabled = true
```
```
[file_lock]
default_expire_hours = 2
use_locked_file_cache = true
```

- in `seafdav.conf`
```
[WEBDAV]
enabled = true
```

- in `seafevents.conf` (only in pro version)
```
[INDEX FILES]
external_es_server = true
es_host = elasticsearch
es_port = 9200
scheme = http
username = elastic
password = ********************
```
docker compose exec elasticsearch /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic

- in `seahub_settings.py` (if onlyoffice enabled)
```
SERVICE_URL = "https://{{ seafile_host }}/" # be careful with https
CSRF_TRUSTED_ORIGINS = [https://{{ seafile_host }}] # no trailing slash
```
```
ENABLE_ONLYOFFICE = True
ONLYOFFICE_JWT_SECRET = '********************************'
VERIFY_ONLYOFFICE_CERTIFICATE = True
ONLYOFFICE_APIJS_URL = 'https://{{ onlyoffice_host }}/web-apps/apps/api/documents/api.js'
ONLYOFFICE_FILE_EXTENSION = ('doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'odt', 'fodt', 'odp', 'fodp', 'ods', 'fods')
ONLYOFFICE_EDIT_FILE_EXTENSION = ('docx', 'pptx', 'xlsx')
ONLYOFFICE_FORCE_SAVE = True

EMAIL_USE_SSL = True
EMAIL_HOST = '{{ mailu_host }}'
EMAIL_HOST_USER = 'admin@{{ mailu_domain }}'
EMAIL_HOST_PASSWORD = '{{ mailu_admin_password }}'
EMAIL_PORT = 465
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
```

Role Variables
--------------

## General

### inventory_version

This README defines librecloud version `3.0.0`. You have to define the variable `inventory_version` equal to the major version (`3`) to show that your inventory is compatible with it.

### acme_email

The email to be used with letsencrypt.

### as_root

Execute all steps as root, otherwise as the user who is connecting with ssh. Default: `True`.

### clear

If set to `True` or a dictionary, clean up workloads (`docker compose down`). If a dictionary, possible keys: `images` (default `False`), `volumes` (default `False`), `build_cache` (default `True`), `networks` (default `True`). The keys define if it should prune it additionaly. Default: `False`.

### development

If set to `True`, the role will make some shortcuts that might only be acceptable in development environments. Default: `False`.

### enable_backup

If set to `True` the backubot container will be deploy to make periodic backups of deployed containers. Default: `True`.

### reset

If set to `True`, the role will remove all running containers and images before deploying. Default: `False`.

### workload_src_path

Path to store the docker-compose and other files. Default: `/src`.

## Backup configuration

### backup_restic_host

URL of the S3 bucket.

### backup_restic_name

Name of the restic repository. Default is `{{ inventory_hostname }}`.

### backup_restic_password

The password to be used to encrypt the restic repository.

### backup_aws_access_key_id

The ID of the S3 account. `AWS_ACCESS_KEY_ID`.

### backup_aws_secret_access_key

The key of the S3 account. `AWS_SECRET_ACCESS_KEY`.

## deploy

The `deploy` variable defines which projects to deploy. It is a list of dictionaries with the structure of the following example:
```yaml
deploy:
  - service: odoo
    variables:
      db_password: "{{ vault_odoo_db_password }}"
      master_password: "{{ vault_odoo_master_password }}"

  - service: odoo
    project: odoo_2

  - service: nextcloud
    variables:
      host: cloud.example.com
      db_password: "{{ vault_nextcloud_db_password }}"
      admin_password: "{{ vault_nextcloud_admin_password }}"
      secret_key: "{{ vault_nextcloud_secret_key }}"

  - service: wordpress
    variables:
      host: example.com
      db_password: "{{ vault_wordpress_db_password }}"

  - service: wordpress
    project: other_wordpress
    variables:
      host: example2.com

  - service: seafile
    variables:
      host: "seafile.example.com"
      admin_email: admin@example.com
      db_password: "{{ vault_seafile_db_password }}"
      admin_password: "{{ vault_seafile_admin_password }}"
      pro: true
      enable_elasticsearch: true

  - service: mailu

mailu_host: mail.example.com
mailu_domain: example.com
mailu_enable:
  - oletools
  - fetchmail
```

If for an item there is no `project` name defined, the `service` name will be used.

The project name of two items cannot be the same.

For the service key possible vaules are: `'odoo'`, `'nextcloud'`, `'onlyoffice'`, `'mailu'`, `'wordpress'`, `'vaultwarden'`, `'seafile'`, `'grafana'`, `'monitoring-agent'`.

Variables for each service are explained below. There are two exceptions: `mailu` and `monitoring-agent` can only be deployed once on a machine, so their variables are not defined inside the enable dictionary but instead are global.

## Global variables

### Mailu configuration

#### mailu_domain

Main mail domain.

#### mailu_host

The hostname used to access mailu. Default: `mail.{{ inventory_hostname }}`.

#### mailu_enable

A list to select which extra services to deploy. Possible vaules: `'webdav'`, `'oletools'`, `'fetchmail'`, `'clamav'`, `'tika'`. Default: `[]`.

#### mailu_secret_key

Set to a randomly generated 16 bytes string.

#### mailu_admin_password

The password for the admin@{{ mailu_domain }} account.

### Monitoring agent configuration

#### node_exporter_host

The hostname used to access node_exporter. Default: `nodeexporter.{{ inventory_hostname }}`.

#### cadvisor_host

The hostname used to access Cadvisor. Default: `cadvisor.{{ inventory_hostname }}`.

#### monitoring_admin_password

The clear password used to interact with the various monitoring services.

#### monitoring_admin_password_hash

The hashed password used to interact with the various monitoring services.

#### loki_host

The hostname of the loki instance to push data. Default: `loki.{{ inventory_hostname }}`.

## Service variables

These variables are defined inside the `variables` dictionary in each service definition.

### Odoo configuration

#### image

The Odoo image to deploy. Obviously there might be some assumptions that are made about the behavior of Odoo's image. This means that that not all images will work, but there's still a chance you want to modify this variable to select a specific version. Default: `registry.gitlab.com/librecoop/librecoop-odoo-docker:latest`

#### host

The hostname used to access Odoo. Default: `odoo.{{ inventory_hostname }}`.

#### db_user

The user that Odoo shall use to connect to the database.

#### db_name

The database name that Odoo shall use.

#### db_password

The password for the database.

#### master_password

The master password for the database.

### Nextcloud configuration

#### host

The hostname used to access Nextcloud. Default: `cloud.{{ inventory_hostname }}`.

#### db_user

The user that nextcloud shall use to connect to the database.

#### db_name

The database name that nextcloud shall use.

#### db_password

The password for the database.

#### admin_password

The password for the admin account.

#### secret_key

The secret key for nextcloud.

### Onlyoffice

#### host

The hostname used to access Onlyoffice. Default: `onlyoffice.{{ inventory_hostname }}`.

#### secret_key

The secret key for onlyoffice.

### Wordpress configuration

#### host

The hostname used to access wordpress. Default: `{{ inventory_hostname }}`.

#### db_user

The user that wordpress shall use to connect to the database.

#### db_name

The database name that wordpress shall use.

#### db_password

The password for the database.

### Vaultwarden configuration

#### host

The hostname used to access vaultwarden. Default: `vault.{{ inventory_hostname }}`.

#### admin_token

The admin token for vaultwarden.

### Seafile configuration

#### host

The hostname used to access seafile. Default: `seafile.{{ inventory_hostname }}`.

#### db_password

The password for the database root user, which will be used the first time to create the other database users.

#### admin_email

The email for the admin account. Default: `admin@{{ inventory_hostname }}`.

#### admin_password

The password for the admin account.

#### pro

Whether to use community or pro version. Default: `False`.

To use the pro version you will need to create an account at https://customer.seafile.com/ and follow the instructions to login to the docker registry.

#### enable_elasticsearch

Whether to deploy elasticsearch too. Default: `False`.

#### elasticsearch_memory_gb

How many GB to allocate to the elasticsearch.

### Grafana configuration

#### grafana_host

The hostname used to access grafana. Default: `grafana.{{ inventory_hostname }}`.

#### loki_host

The hostname used to access loki. Default: `loki.{{ inventory_hostname }}`.

#### prometheus_targets

A list of url to pool monitoring data from. Default: `['node-exporter:9100', 'cadvisor:8080']`.

#### monitoring_http_endpoints

A list of urls to monitor if they are up. Default: `None`

## Extra workloads

It is possible to define workloads that are not included by default, they follow this scheme:
```yaml
extra_workloads:
  - name: name
    repo: https://giturl.com/repo.git
    environment:
      ENV_VARIABLE: value
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.deploy
      vars:
        odoo_host: "odoo.librecoop.es"
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
